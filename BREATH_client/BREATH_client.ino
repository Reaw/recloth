#include <SPI.h>
#include <WiFiNINA.h>
#include <MFRC522.h>
#include <ServoController.h>
#include "server_data.h"

/* RFID */
#define RST_PIN         6
#define SS_PIN          7
MFRC522 mfrc522(SS_PIN, RST_PIN);  // Create MFRC522 instance
String twinUID1 = " 51 a1 b3 1c";
String twinUID2 = " ee 59 66 60";
bool rfidTagPresent = false;
bool rfidTagPrevPresent1;
bool rfidTagPrevPresent2;
unsigned short ms;
unsigned short delayRFIDCheck = 1000U;

#define pinServo1 0
#define pinServo2 1
unsigned short timeToReachSpeedServo1 = 5000U;
unsigned short timeToReachSpeedServo2 = 5000U;
unsigned short wifiSpeed = 95U;
unsigned short rfidSpeed = 140U;

/* Connection */
char ssid[] = SECRET_SSID;
char pass[] = SECRET_PASSWORD;
int status = WL_IDLE_STATUS;
WiFiClient client;

unsigned short wifiPattern[32] = {90, 80, 100, 70, 110, 60,
                                120, 50, 130, 40, 140, 30,
                                160, 20, 10, 170, 0, 180,
                                10, 170, 160, 20, 140, 30,
                                130, 40, 120, 50, 110, 60,
                                100, 70};

unsigned short rfidPattern[2] = {70, 110}; // super energetic

/* ServoController */
/* ServoController */
ServoController servoController1 = ServoController(pinServo1, timeToReachSpeedServo1);
ServoController servoController2 = ServoController(pinServo2, timeToReachSpeedServo2, wifiPattern, 32);

void setup() {
  initWifi();
}

void loop() {
  servoController1.loop();
  servoController2.loop();

  if (checkCycle(&ms, delayRFIDCheck)){
    checkRFID();
  }

  updateConnectionStatus();

  //if we are not connected we search networks and try to connect
  if (status != WL_CONNECTED
      && !servoController1.patternIsOn
      && !servoController2.patternIsOn) {
        searchForCorrespondingNetwork();
  }
}

void initWifi(){
  // check for the WiFi module:
  if (WiFi.status() == WL_NO_MODULE) {
    Serial.println("Communication with WiFi module failed!");
    // don't continue
    while (true);
  }

  String fv = WiFi.firmwareVersion();
  if (fv < WIFI_FIRMWARE_LATEST_VERSION) {
    Serial.println("Please upgrade the firmware");
  }

  status = WiFi.status();

  // print your MAC address:
  byte mac[6];
  WiFi.macAddress(mac);
  Serial.print("MAC: ");
  printMacAddress(mac);
}

void searchForCorrespondingNetwork() {
  // scan for nearby networks:
  Serial.println("** Scan Networks **");
  int numSsid = WiFi.scanNetworks();
  if (numSsid == -1)
  {
    Serial.println("Couldn't get a WiFi connection");
    while (true);
  }

  // print the number of networks seen:
  Serial.print("Number of available networks: ");
  Serial.println(numSsid);

  // go through all networks found and print the right one
  for (int thisNet = 0; thisNet < numSsid; thisNet++) {
    //Check if we find our hotspot
    String foundSSID(WiFi.SSID(thisNet));
    String searchedSSID(SECRET_SSID);
    if (foundSSID == searchedSSID) {
      Serial.println("** FOUND CORRESPONDING WIFI HOTSPOT **");
      Serial.print("Signal: ");
      Serial.print(WiFi.RSSI(thisNet));
      Serial.print(" dBm");
      Serial.print("\tChannel: ");
      Serial.print(WiFi.channel(thisNet));
      byte bssid[6];
      Serial.print("\t\tBSSID: ");
      printMacAddress(WiFi.BSSID(thisNet, bssid));
      Serial.print("\tEncryption: ");
      printEncryptionType(WiFi.encryptionType(thisNet));
      Serial.print("\t\tSSID: ");
      Serial.println(WiFi.SSID(thisNet));
      //Init connection
      attemptConnection();
      //exit loop (ugly I know)
      thisNet = numSsid;
    }
    Serial.flush();
  }
  Serial.println();
}

void attemptConnection() {
  // attempt to connect to Wifi network:
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print("Attempting to connect to SSID: ");
    Serial.println(SECRET_SSID);
    // Connect to WPA/WPA2 network. Change this line if using open or WEP network:
    WiFi.begin(SECRET_SSID, SECRET_PASSWORD);

    // wait 1 seconds for connection:
    //delay(1000);
  }
  Serial.println("Connected to wifi");
  printWifiStatus();
}

void updateConnectionStatus() {
  // compare the previous status to the current status
  if (status != WiFi.status()) {
    // it has changed update the variable
    status = WiFi.status();

    if (status == WL_CONNECT_FAILED ||
        status == WL_CONNECTION_LOST ||
        status == WL_DISCONNECTED) {
      handleDisconnection();
    }
    else if (status == WL_CONNECTED) {
      handleConnection();
    }
  }
}

//handler called when a device is connected
void handleConnection() {
  Serial.println("Device connected to AP");
  //ServoController 1
  servoController1.setPattern(wifiPattern, 32);
  servoController1.startPattern();
  servoController1.setDesiredSpeed(wifiSpeed);
  //ServoController 2
  servoController2.setPattern(wifiPattern, 32);
  servoController2.startPattern();
  servoController2.setDesiredSpeed(wifiSpeed);
}

//handler called when a device is disconnected
void handleDisconnection() {
  Serial.println("Device disconnected from AP");
  servoController1.stopPattern();
  servoController2.stopPattern();
}

//Checks if device is connected or disconnected
void checkConnections() {
  // compare the previous status to the current status
  if (status != WiFi.status()) {
    // it has changed update the variable
    status = WiFi.status();
    if (status == WL_AP_CONNECTED) {
      // a device has connected to the AP
      handleConnection();
    } else {
      // a device has disconnected from the AP
      handleDisconnection();
    }
  }
}

//Checks when the other device is in RFID range
void checkRFID(){
  rfidTagPrevPresent2 = rfidTagPrevPresent1;
  rfidTagPrevPresent1 = rfidTagPresent;
  // When a card is here with readable uid
  if (mfrc522.PICC_IsNewCardPresent() && mfrc522.PICC_ReadCardSerial() ) {
    String readUID = "";
    for (byte i = 0; i < mfrc522.uid.size; i++) {
      readUID += String(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " ");
      readUID += String(mfrc522.uid.uidByte[i], HEX);
    }
    rfidTagPresent = (readUID == twinUID1 || readUID == twinUID2);
    Serial.println(readUID);
  }
  else{
    rfidTagPresent = false;
  }

  if (rfidTagPresent && !rfidTagPrevPresent1 && !rfidTagPrevPresent2) {
    handleRFIDStart();
  }
  else if (!rfidTagPresent && !rfidTagPrevPresent1 && rfidTagPrevPresent2) { // needs to be false 2 times in a row to disconnect because detections ping pongs (0, 1, 0, 1 ...)
    handleRFIDStop();
  }
}

void handleRFIDStart(){
  Serial.println("RFID ON");
  //ServoController 1
  servoController1.setPattern(rfidPattern, 2);
  servoController1.setDesiredSpeed(rfidSpeed);
  //ServoController 2
  servoController2.setPattern(rfidPattern, 2);
  servoController2.setDesiredSpeed(rfidSpeed);
}

void handleRFIDStop(){
  Serial.println("RFID OFF");
  servoController1.setPattern(wifiPattern, 32);
  servoController1.setDesiredSpeed(wifiSpeed);
  servoController2.setPattern(wifiPattern, 32);
  servoController2.setDesiredSpeed(wifiSpeed);
}

boolean checkCycle(unsigned short * lastMillis, unsigned int cycle) {
  unsigned short currentMillis = millis();
  if (currentMillis - *lastMillis >= cycle)
  {
    *lastMillis = currentMillis;
    return true;
  }
  else
    return false;
}

/** Printing functions **/

void printMacAddress(byte mac[]) {
  for (int i = 5; i >= 0; i--) {
    if (mac[i] < 16) {
      Serial.print("0");
    }
    Serial.print(mac[i], HEX);
    if (i > 0) {
      Serial.print(":");
    }
  }
  Serial.println();
}

void printWifiStatus() {
  // print the SSID of the network you're attached to:
  Serial.print("SSID: ");
  Serial.println(WiFi.SSID());

  // print your board's IP address:
  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);

  // print the received signal strength:
  long rssi = WiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.print(rssi);
  Serial.println(" dBm");
}

void printEncryptionType(int thisType) {
  // read the encryption type and print out the name:
  switch (thisType) {
    case ENC_TYPE_WEP:
      Serial.print("WEP");
      break;
    case ENC_TYPE_TKIP:
      Serial.print("WPA");
      break;
    case ENC_TYPE_CCMP:
      Serial.print("WPA2");
      break;
    case ENC_TYPE_NONE:
      Serial.print("None");
      break;
    case ENC_TYPE_AUTO:
      Serial.print("Auto");
      break;
    case ENC_TYPE_UNKNOWN:
    default:
      Serial.print("Unknown");
      break;
  }
}
