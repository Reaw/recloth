#include <WiFiNINA.h>
#include <ServoController.h>
#include <SPI.h>
#include <MFRC522.h>
#include "server_data.h"

/* RFID */
#define RST_PIN         6
#define SS_PIN          7
MFRC522 mfrc522(SS_PIN, RST_PIN);  // Create MFRC522 instance
String twinUID1 = " b9 07 ff 2a";
String twinUID2 = " 8e 7d 36 60";

bool rfidTagPresent = false;
bool rfidTagPrevPresent1;
bool rfidTagPrevPresent2;
unsigned short ms;
unsigned short delayRFIDCheck = 1000U;

#define pinServo1 0
#define pinServo2 1
unsigned short timeToReachSpeedServo1 = 5000U;
unsigned short timeToReachSpeedServo2 = 5000U;
unsigned short wifiSpeed = 95U;
unsigned short rfidSpeed = 140U;

/* Connection */
char ssid[] = SECRET_SSID;
char pass[] = SECRET_PASSWORD;
int status = WL_IDLE_STATUS;  // Setting the Wifi to default status

unsigned short wifiPattern[32] = {90, 80, 100, 70, 110, 60,
                                120, 50, 130, 40, 140, 30,
                                160, 20, 10, 170, 0, 180,
                                10, 170, 160, 20, 140, 30,
                                130, 40, 120, 50, 110, 60,
                                100, 70};

unsigned short rfidPattern[2] = {70, 110}; // super energetic

/* ServoController */
ServoController servoController1 = ServoController(pinServo1, timeToReachSpeedServo1);
ServoController servoController2 = ServoController(pinServo2, timeToReachSpeedServo2, wifiPattern, 32);

/*** SETUP ***/
void setup() {
  initWifi();
  SPI.begin();      // Init SPI bus
  mfrc522.PCD_Init();   // Init MFRC522
  mfrc522.PCD_DumpVersionToSerial();  // Show details of PCD - MFRC522 Card Reader details
  Serial.println("Setup : DONE");
  ms = millis();
}

/*** LOOP ***/
void loop() {
  checkConnections();
  if (checkCycle(&ms, delayRFIDCheck)){
    checkRFID();
  }

  servoController1.loop();
  servoController2.loop();
}

void initWifi() {
  Serial.println("Access Point Web Server");

  // check for the WiFi module:
  if (WiFi.status() == WL_NO_MODULE) {
    Serial.println("Communication with WiFi module failed!");
    // don't continue
    while (true);
  }

  String firmware_version = WiFi.firmwareVersion();
  if (firmware_version < WIFI_FIRMWARE_LATEST_VERSION) {
    Serial.println("Please upgrade the firmware");
  }

  // print the network name (SSID);
  Serial.print("Creating access point named: ");
  Serial.println(ssid);

  //Initializes the WiFi101 library in Access Point (AP) mode.
  status = WiFi.beginAP(ssid, pass);
  if (status != WL_AP_LISTENING) {
    Serial.println("Creating access point failed");
    // don't continue
    while (true);
  }
}

//Checks if device is connected or disconnected
void checkConnections() {
  // compare the previous status to the current status
  if (status != WiFi.status()) {
    // it has changed update the variable
    status = WiFi.status();
    if (status == WL_AP_CONNECTED) {
      // a device has connected to the AP
      handleConnection();
    } else {
      // a device has disconnected from the AP
      handleDisconnection();
    }
  }
}

//handler called when a device is connected
void handleConnection() {
  Serial.println("Device connected to AP");

  //ServoController 1
  servoController1.setPattern(wifiPattern, 32);
  servoController1.startPattern();
  servoController1.setDesiredSpeed(wifiSpeed);
  //ServoController 2
  servoController2.setPattern(wifiPattern, 32);
  servoController2.startPattern();
  servoController2.setDesiredSpeed(wifiSpeed);
}

//handler called when a device is disconnected
void handleDisconnection() {
  Serial.println("Device disconnected from AP");
  servoController1.stopPattern();
  servoController2.stopPattern();
}

//Checks when the other device is in RFID range
void checkRFID(){
  rfidTagPrevPresent2 = rfidTagPrevPresent1;
  rfidTagPrevPresent1 = rfidTagPresent;
  // When a card is here with readable uid
  if (mfrc522.PICC_IsNewCardPresent() && mfrc522.PICC_ReadCardSerial() ) {
    String readUID = "";
    for (byte i = 0; i < mfrc522.uid.size; i++) {
      readUID += String(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " ");
      readUID += String(mfrc522.uid.uidByte[i], HEX);
    }
    rfidTagPresent = (readUID == twinUID1 || readUID == twinUID2);
    Serial.println(readUID);
  }
  else{
    rfidTagPresent = false;
  }

  if (rfidTagPresent && !rfidTagPrevPresent1 && !rfidTagPrevPresent2) {
    handleRFIDStart();
  }
  else if (!rfidTagPresent && !rfidTagPrevPresent1 && rfidTagPrevPresent2) { // needs to be false 2 times in a row to disconnect because detections ping pongs (0, 1, 0, 1 ...)
    handleRFIDStop();
  }
}

void handleRFIDStart(){
  Serial.println("RFID ON");
  //ServoController 1
  servoController1.setPattern(rfidPattern, 2);
  servoController1.setDesiredSpeed(rfidSpeed);
  //ServoController 2
  servoController2.setPattern(rfidPattern, 2);
  servoController2.setDesiredSpeed(rfidSpeed);
}

void handleRFIDStop(){
  Serial.println("RFID OFF");
  servoController1.setPattern(wifiPattern, 32);
  servoController1.setDesiredSpeed(wifiSpeed);
  servoController2.setPattern(wifiPattern, 32);
  servoController2.setDesiredSpeed(wifiSpeed);
}

boolean checkCycle(unsigned short * lastMillis, unsigned int cycle) {
  unsigned short currentMillis = millis();
  if (currentMillis - *lastMillis >= cycle)
  {
    *lastMillis = currentMillis;
    return true;
  }
  else
    return false;
}
