# ReCloth
Source code and technical description of the ReCloth project of connected wearables. This project is part of the [Re:tangent](https://hci.sbg.ac.at/sites/retangent/) research project of the [Center for Human-Computer Interaction](https://hci.sbg.ac.at) - University of Salzburg.

## Project summary
The aim of this project is to experiment on connected wearables to embody negative of people's feelings in  dislocated relationships (separated friends, family...). We are currently experimenting, and building 2 prototypes (pairs) of  wearables reacting to dislocation:
- BREATH: a pair of wearables with servomotors moving the textile
- WARMTH: a pair of wearables with heatpads heating the textiles

The prototypes are described in this pictorial published at TEI 2021 : [https://dl.acm.org/doi/10.1145/3430524.3446071](https://dl.acm.org/doi/10.1145/3430524.3446071)

This repository contains the source code for the MKR1010 WiFi Arduino boards controlling the prototypes. The code uses this tool for controlling the servos : [https://gitlab.com/Reaw/servocontroller](https://gitlab.com/Reaw/servocontroller)

## What this prototype currently does
Currently, the two prototypes uses WiFi connection to detect when two people wearing the wearables are getting closer. BREATH also includes RFID sensors and chips for detecting "hug proximity".

 The use of WiFi technology for dislocation is limited and can be discussed. Ideas and experimentations on how to improve the prototype or make better new ones can be read below under "Ideas / experimentations and their outcomes".

## BREATH
The BREATH prototype is a pair of 2 wearable artifacts with embedded electronics for **moving** the textile with a breathing pattern. The embedded electronics are as shown below, one artifact executes the BREATH_client Arduino code the other executes the BREATH_server Arduino code.

### Sketch of the electronics
This is a simple sketch of the BREATH system showing the connections, including the RFID chip, the MKR1010 board, and the servos.
![Sketch of the system](./images/circuit_schematics_bb.png "Sketch of the system")
### Schematics of the electronics
This is a simple schematics of BREATH the system showing electronics a bit more in detail.
![Schematic of the system](./images/circuit_schematics_schem.png "Schematic of the system")

## WARMTH
The WARMTH prototype is a pair of 2 wearable artifacts with embedded electronics for **heating** the textile with a breathing pattern. The embedded electronics are as shown below, one artifact executes the WARMTH_client Arduino code the other executes the WARMTH_server Arduino code.

### Sketch of the electronics
This is a simple sketch of the WARMTH system showing the connections, including the heating pads, the MKR1010 board, and the external 9V battery.
![Sketch of the system](./images/warmth_schematics_bb.png "Sketch of the system")
### Schematics of the electronics
This is a simple schematics of the WARMTH system showing the connections, including the heating pads, the MKR1010 board, and the external 9V battery.


## Ideas / experimentations and their outcomes

###### Using RSSI to approximate the distance between the two prototypes
>This would have been a possible solution for increasing the intensity/speed of the cloth movement the closer people wearing the prototype would get. It theoretically could have worked, but the limitations of a single core processing unit (Arduino) makes it overly complicated and "space-taking" to implement (multiple boards). We would need to scan networks constantly to get the RSSI in dB, and then communicate it to the server. This first operation already takes roughly 2 seconds by itself, which makes it impossible to keep on moving the servos. (Issue described [here](https://github.com/esp8266/Arduino/issues/132#issuecomment-97994661))
###### Using GPS to improve the reaction of cloth to dislocation
>By adding real time GPS localisation we could increase the range of reaction of the wearable prototypes. Which means we could make cloth react more accurately to dislocation. For example, we could :
>
> - detect perfectly  when a person wearing a prototype is leaving to another town, region, state etc...
> - detect if a person is close and check if the person is really heading towards the other person or only "hanging out" in a close location.
>
>We found some problems related to this solution,
>
> - it needs permanent internet connection. Still this could be solved by connecting the board to a smartphone.
> - the prototype would then be meant to be worn for a very long time, which would mean including very long lasting batteries. This would make the prototypes a lot heavier.
Or with smaller batteries, it would force the users to charge them regularly, which would make the use of these clothes less natural.
###### Using RFID sensors to detect highest proximity
>We thought of using RFID sensors to detect when two people wearing the prototype would get so close to each other that they could touch. An example of this proximity is when people hug. RFID could be a solution for this, and is still being experimented.
###### Including sounds to strengthen negative feelings due to dislocation
>To investigate...

## Credits

*Project conducted by [Mascha Beuthel](https://hci.sbg.ac.at/person/beuthel/), and supervised by [Verena Fuchsberger](https://hci.sbg.ac.at/person/fuchsberger/)*

*Code by [Philippe Bentegeac](https://philippe.bentegeac.com)*

*Sewing by [Mascha Beuthel](https://hci.sbg.ac.at/person/beuthel/)*

*Electronics by [Philippe Bentegeac](https://philippe.bentegeac.com), [Mascha Beuthel](https://hci.sbg.ac.at/person/beuthel/), [Artur Lupp](https://hci.sbg.ac.at/person/lupp/)*

*Schematics by [Philippe Bentegeac](https://philippe.bentegeac.com) with Fritzing using custom asset for [RFID RC522 module](https://forum.fritzing.org/t/rfid-rc522-module-part-for-13-56-mhz-card/5711), and [Heating Pad](https://forum.fritzing.org/t/silicon-heating-pads/2635)*
