#include <SPI.h>
#include <WiFiNINA.h>
#include <MFRC522.h>
#include <ServoController.h>
#include "server_data.h"

#define pinMOSFET 2

/* Connection */
char ssid[] = SECRET_SSID;
char pass[] = SECRET_PASSWORD;
int status = WL_IDLE_STATUS;
WiFiClient client;

void setup() {
  initWifi();
}

void loop() {
  updateConnectionStatus();

  //if we are not connected we search networks and try to connect
  if (status != WL_CONNECTED) {
        searchForCorrespondingNetwork();
  }
}

void initWifi(){
  // check for the WiFi module:
  if (WiFi.status() == WL_NO_MODULE) {
    Serial.println("Communication with WiFi module failed!");
    // don't continue
    while (true);
  }

  String fv = WiFi.firmwareVersion();
  if (fv < WIFI_FIRMWARE_LATEST_VERSION) {
    Serial.println("Please upgrade the firmware");
  }

  status = WiFi.status();

  // print your MAC address:
  byte mac[6];
  WiFi.macAddress(mac);
  Serial.print("MAC: ");
  printMacAddress(mac);
}

void searchForCorrespondingNetwork() {
  // scan for nearby networks:
  Serial.println("** Scan Networks **");
  int numSsid = WiFi.scanNetworks();
  if (numSsid == -1)
  {
    Serial.println("Couldn't get a WiFi connection");
    while (true);
  }

  // print the number of networks seen:
  Serial.print("Number of available networks: ");
  Serial.println(numSsid);

  // go through all networks found and print the right one
  for (int thisNet = 0; thisNet < numSsid; thisNet++) {
    //Check if we find our hotspot
    String foundSSID(WiFi.SSID(thisNet));
    String searchedSSID(SECRET_SSID);
    if (foundSSID == searchedSSID) {
      Serial.println("** FOUND CORRESPONDING WIFI HOTSPOT **");
      Serial.print("Signal: ");
      Serial.print(WiFi.RSSI(thisNet));
      Serial.print(" dBm");
      Serial.print("\tChannel: ");
      Serial.print(WiFi.channel(thisNet));
      byte bssid[6];
      Serial.print("\t\tBSSID: ");
      printMacAddress(WiFi.BSSID(thisNet, bssid));
      Serial.print("\tEncryption: ");
      printEncryptionType(WiFi.encryptionType(thisNet));
      Serial.print("\t\tSSID: ");
      Serial.println(WiFi.SSID(thisNet));
      //Init connection
      attemptConnection();
      //exit loop (ugly I know)
      thisNet = numSsid;
    }
    Serial.flush();
  }
  Serial.println();
}

void attemptConnection() {
  // attempt to connect to Wifi network:
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print("Attempting to connect to SSID: ");
    Serial.println(SECRET_SSID);
    // Connect to WPA/WPA2 network. Change this line if using open or WEP network:
    WiFi.begin(SECRET_SSID, SECRET_PASSWORD);

    // wait 1 seconds for connection:
    //delay(1000);
  }
  Serial.println("Connected to wifi");
  printWifiStatus();
}

void updateConnectionStatus() {
  // compare the previous status to the current status
  if (status != WiFi.status()) {
    // it has changed update the variable
    status = WiFi.status();

    if (status == WL_CONNECT_FAILED ||
        status == WL_CONNECTION_LOST ||
        status == WL_DISCONNECTED) {
      handleDisconnection();
    }
    else if (status == WL_CONNECTED) {
      handleConnection();
    }
  }
}

//handler called when a device is connected
void handleConnection() {
  Serial.println("Device connected to AP");
  startHeat();
}

//handler called when a device is disconnected
void handleDisconnection() {
  Serial.println("Device disconnected from AP");
  stopHeat();
}

void startHeat(){
  Serial.println("ON");
  analogWrite(2, 255);
}

void stopHeat(){
  Serial.println("OFF");
  analogWrite(2, 0);
}

//Checks if device is connected or disconnected
void checkConnections() {
  // compare the previous status to the current status
  if (status != WiFi.status()) {
    // it has changed update the variable
    status = WiFi.status();
    if (status == WL_AP_CONNECTED) {
      // a device has connected to the AP
      handleConnection();
    } else {
      // a device has disconnected from the AP
      handleDisconnection();
    }
  }
}

/** Printing functions **/
void printMacAddress(byte mac[]) {
  for (int i = 5; i >= 0; i--) {
    if (mac[i] < 16) {
      Serial.print("0");
    }
    Serial.print(mac[i], HEX);
    if (i > 0) {
      Serial.print(":");
    }
  }
  Serial.println();
}

void printWifiStatus() {
  // print the SSID of the network you're attached to:
  Serial.print("SSID: ");
  Serial.println(WiFi.SSID());

  // print your board's IP address:
  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);

  // print the received signal strength:
  long rssi = WiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.print(rssi);
  Serial.println(" dBm");
}

void printEncryptionType(int thisType) {
  // read the encryption type and print out the name:
  switch (thisType) {
    case ENC_TYPE_WEP:
      Serial.print("WEP");
      break;
    case ENC_TYPE_TKIP:
      Serial.print("WPA");
      break;
    case ENC_TYPE_CCMP:
      Serial.print("WPA2");
      break;
    case ENC_TYPE_NONE:
      Serial.print("None");
      break;
    case ENC_TYPE_AUTO:
      Serial.print("Auto");
      break;
    case ENC_TYPE_UNKNOWN:
    default:
      Serial.print("Unknown");
      break;
  }
}
