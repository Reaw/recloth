#include <WiFiNINA.h>
#include <ServoController.h>
#include <SPI.h>
#include "server_data.h"

#define pinMOSFET 2

/* Connection */
char ssid[] = SECRET_SSID;
char pass[] = SECRET_PASSWORD;
int status = WL_IDLE_STATUS;  // Setting the Wifi to default status

/*** SETUP ***/
void setup() {
  pinMode(2,OUTPUT);
  initWifi();
  SPI.begin();      // Init SPI bus
  Serial.println("Setup : DONE");
}

/*** LOOP ***/
void loop() {
  checkConnections();
}

void initWifi() {
  Serial.println("Access Point Web Server");

  // check for the WiFi module:
  if (WiFi.status() == WL_NO_MODULE) {
    Serial.println("Communication with WiFi module failed!");
    // don't continue
    while (true);
  }

  String firmware_version = WiFi.firmwareVersion();
  if (firmware_version < WIFI_FIRMWARE_LATEST_VERSION) {
    Serial.println("Please upgrade the firmware");
  }

  // print the network name (SSID);
  Serial.print("Creating access point named: ");
  Serial.println(ssid);

  //Initializes the WiFi101 library in Access Point (AP) mode.
  status = WiFi.beginAP(ssid, pass);
  if (status != WL_AP_LISTENING) {
    Serial.println("Creating access point failed");
    // don't continue
    while (true);
  }
}

//Checks if device is connected or disconnected
void checkConnections() {
  // compare the previous status to the current status
  if (status != WiFi.status()) {
    // it has changed update the variable
    status = WiFi.status();
    if (status == WL_AP_CONNECTED) {
      // a device has connected to the AP
      handleConnection();
    } else {
      // a device has disconnected from the AP
      handleDisconnection();
    }
  }
}

//handler called when a device is connected
void handleConnection() {
  Serial.println("Device connected to AP");
  startHeat();
}

//handler called when a device is disconnected
void handleDisconnection() {
  Serial.println("Device disconnected from AP");
  stopHeat();
}

void startHeat(){
  Serial.println("ON");
  analogWrite(2, 255);
}

void stopHeat(){
  Serial.println("OFF");
  analogWrite(2, 0);
}
